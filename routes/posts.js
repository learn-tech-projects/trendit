var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('posts', { title: 'Detailed Post' });
});

router.get('/get/:postid', function(req, res, next) {
	// Access postId via: req.params.postId
  res.render('detail-post', { title: 'Detailed Post' });
});

router.get('/create', function(req, res, next) {
	// Access postId via: req.params.postId
  res.render('create-post', { title: 'Add New Post' });
});

module.exports = router;
