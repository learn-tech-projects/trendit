var express = require('express');
var router = express.Router();

let userController = require('../controller/userController.js')

/* GET users listing. То есть тут берется контроллер userController и используется функция из этого файла */
router.get('/all', function(req, res, next) {
   userController.list_all_users(req,res)
});


/* GET user by id. */
router.get('/:userId', function(req, res, next) {
   res.render('profile', { title: 'User profile' });
});

module.exports = router;
