/* Тут запрашивается модель для пользователя */
let User = 	require('../model/userModel.js')

/* Тут идет запрос всех пользовтелей. Создается функция которая просто экспортируется */
let list_all_users = (req,res) => {

	// Используется встроенная функция внутри модели User
	User.getUsers( (err, users)=>{

				if(err){
					res.send(err)
					console.log('res:',users )
				}

				/*Команда для рендера необходимой страницы сразу идет тут
					Если так подумать, то данная схема очень схожая с тем, что было в Laravel
					То есть паттерн тот же. MVC
				*/
				res.render('profiles',{title:"All users", users:users})
	})

}

/* Добавления нового пользователя пока включает в себя только проверку того, пришли ли определенные данные,
  но думаю что стоит учитывать, что параметров будет больше и соотвественно проверки должны быть
  более вариативными */
let create_new_user = (req, res)=>{
	let new_user = new User(req.body)

	if(!new_user.login || new_user.password || new_user.email){
		
		res.status(400).send({
				error:true,
				message:"Please provide login/password/email"
		})

	}
	else{
				User.createUser(new_user , (err,user)=>{
					
					if(err){
						res.send(err)
					}

					res.json(user)

				})
	}


}

let getUser  = (req,res) =>{
	User.getUserById(req.params.userId , (err , user)=>{
		
		if(err){
			res.send(err)
		}
		res.json(user)

	})
}

let updateUser = (req,res)=>{
	Task.updateById(req.params.userId , new User(req.body) , (err,user)=>{

		if(err){
			res.send(err)
		}
		res.json(user)

	})
}


let removeUser  = (req,res)=>{
	User.removeById(req.params.userId , (err,user)=>{

		if(err){
			res.send(err)
		}
		res.json(user)		

	})
}

/* Тут они экскортируются и потом их сразу можно использовать в других местах */
module.exports = {
  list_all_users,
	create_new_user,
};