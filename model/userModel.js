// Тут отдельно подклчюается к БД, так как необходимо работать с ней в первую очередь
let sql = require('../db.js')

// Конструктор для данного класса
let User = function(user){
	// this.user = user.user;
	this.login = user.login;

	//todo: тут как я понимаю нужна опрделенная функция, чтобы сделать пароль более защищенным, 
	// либо надо обрабатывать это до того, как она доберется до модели и данного конструктора
	this.password = user.password;
	this.email = user.email;
}

// Пока созданы два метода. СОздание пользователя и получение всех пользователей
// Получается тут через sql.query отправляется запрос в Бд
// После чего риходит ответ  и обрабатывается данный запрос
User.createUser = function createUser (newUser, result){
	sql.query('insert into users set ?',newUser, (err,res)=>{
			
			if(err){
				console.log("error: ",err)
				result(err,null)
			}
			else{
				console.log(res.insertId)
				result(null,res.insertId)
			}


	})
}

// Запрос для получения пользовтелей
User.getUsers = function getUsers(result){
	sql.query("Select * from users ", (err,res)=>{

			if(err) {
				console.log("error: ", err);
				result(null, err);
			}
			else{
				console.log('users : ', res);  
			
				result(null, res);
			}

	})
}

User.getUserById = function getUserById(userId ,result) {
	sql.query('select user from users where id = ? ' , userId , (err,res)=>{
		
		if(err){
			console.log(err)
			result(err,null)
		}
		else{
			result(null,res)
		}

	})

}

User.updateById  = function updateById (userId , user , result){
	sql.query('update users set user = ? where id+ ?' , [user.user, userId]  (err,res)=>{

		if(err){
			console.log(err)
			result(null,err)
		}
		else{
			result(null, err)
		}

	})
}

User.removeById = function removeById(userId , result){
	sql.query('delete from users where id = ? ' , userId , (err,res)=>{

		if(err){
			console.log(err)
			result(null,err)
		}
		else{
			result(null, err)
		}

	})
}

// Тут эскортируется уже класс/объект с готовыми функциями
module.exports = User;
